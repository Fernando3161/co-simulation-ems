'''
Created on 16.04.2021

@author: Fernando Penaherrera
'''

def get_fraction_of_hour_timeseries(data):
    return data.index.to_series().diff().min().total_seconds() / 3600

if __name__ == '__main__':
    pass