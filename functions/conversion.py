'''
Created on 23.03.2021

@author: Fernando Penaherrera
'''
from scipy.constants.constants import zero_Celsius, hour


def kilo_to_mega(arg):
    return arg / 1000


def celsius_to_kelvin(arg):
    return zero_Celsius + arg


def kelvin_to_celsius(arg):
    return arg - zero_Celsius


def kJ_to_MWh(arg):
    return kilo_to_mega(arg / hour)


def kJ_to_kWh(arg):
    return (arg / hour)


if __name__ == '__main__':
    print(celsius_to_kelvin(0))
    print(hour)
    pass
