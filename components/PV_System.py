'''
Created on 29.03.2021

@author: Fernando Penaherrera
'''
from pandas.core.frame import DataFrame
from components.photovoltaic import PVpanel
from data.data_preprocessing import gather_and_process_data
from scipy import radians
from datetime import datetime


class PV_System(PVpanel):
    '''
    Converts the PV_Panels and the data_frame with solar energy data
    into a time series with energy flows
    '''

    def __init__(self, wetter_data,
                 lat=0, area=1, efficiency=0.23,
                 el_tilt=0, az_tilt=0,):
        self.area = area
        self.efficiency = efficiency
        self.el_tilt = radians(el_tilt)  # elevation
        self.az_tilt = radians(az_tilt)  # azimuth
        self.lat = radians(lat)  # latitude
        self.wetter_data = wetter_data

    def _get_solar_radiation(self):
        return self.wetter_data[["Solar Radiation"]]

    @property
    def get_pv_production(self):
        data = self._get_solar_radiation()
        data["DateString"] = list(data.index.strftime("%Y-%m-%d %h:%m:%s"))
        data["Production"] = data.apply(
            lambda x: PVpanel(start_date=x["DateString"]).power(
                x["Solar Radiation"]), axis=1)

        return data["Production"]


if __name__ == '__main__':
    # 1000m2 = 213 kW @STC
    START = "2021-02-10"
    END = "2021-02-19"
    wetter_data = gather_and_process_data(start_date=START,
                                          end_date=END)
    
    pv_sys = PV_System(wetter_data,area=1000,az_tilt=30)
    prod = pv_sys.get_pv_production
    print(pv_sys.az_tilt)
    import matplotlib.pyplot as plt
    prod.plot()
    plt.show()
    # ===========================================================================
    # import matplotlib.pyplot as plt
    # pv_sys.get_pv_production.plot()
    # plt.show()
    # ===========================================================================
