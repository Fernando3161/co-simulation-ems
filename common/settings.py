'''
Created on 29.03.2021

@author: Fernando Penaherrera
'''
import os


def get_project_root():
    """Return the path to the project root directory.

    :return: A directory path.
    :rtype: str
    """
    return os.path.realpath(os.path.join(
        os.path.dirname(__file__),
        os.pardir,
    ))


BASE_DIR = get_project_root()
DATA_DIR = get_project_root() + "\\data"
MODEL_DIR = get_project_root() + "\\model"
RESULTS_DIR = get_project_root() + "\\results"

if __name__ == '__main__':
    pass
