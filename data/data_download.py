'''
Created on 08.03.2021

@author: Fernando Penaherrera
'''
import urllib.request as rq
import re

# TODO
# From the energy data it is better to brute force and download all of the year...
# the weeks are incomplete and they may mislead the info...
# need to work a bit on this
import os.path
from common.settings import DATA_DIR

def convert_german_date(date="30.12.2021"):
    return (date[6:] + "-" + date[3:5] + "-" + date[0:2])

def download_data_year(date="2021-03-01"):
    r = re.compile('.{4}')
    if r.match(date) is None:
        print("Wrong date format. Use YYYY")
        return None

    month = 1
    exists = True
    while exists:
        try:
            file_name = DATA_DIR + "\\weather\\weather{}.txt".format(date[2:4]
                                                                     + str(month).zfill(2))
            if not os.path.isfile(file_name):
                WETTER_DATA = "https://uol.de/dez4/wetter/ausgabe.php?datei="\
                    + "wetter{}{}.txt".format(date[2:4], str(month).zfill(2))
                rq.urlretrieve(WETTER_DATA, file_name)
            month += 1
        except BaseException:
            exists = False

    week = 1
    exists = True
    while exists:
        try:
            file_name = DATA_DIR + "\\generation\\energy_price{}.json".format(
                date[2:4] + "w" + str(week).zfill(2))
            if not os.path.isfile(file_name):
                ENERGY_PRICE = "https://energy-charts.info/charts/price_spot_market"\
                    + "/data/de/week_{}_{}.json".format(date[0:4],
                                                        str(week).zfill(2))
                POWER_GENERATION = "https://energy-charts.info/charts/power/data/de/"\
                    + "week_{}_{}.json".format(date[0:4], str(week).zfill(2))

                # download data from URLs
                rq.urlretrieve(ENERGY_PRICE,
                               DATA_DIR + "\\generation\\energy_price{}.json".format(
                                   date[2:4] + "w" + str(week).zfill(2)))

                rq.urlretrieve(POWER_GENERATION,
                               DATA_DIR + "\\generation\\power_generation{}.json".format(
                                   format(date[2:4] + "w" + str(week).zfill(2))))
            week += 1
        except BaseException:
            exists = False


if __name__ == '__main__':
    download_data_year("2021")
