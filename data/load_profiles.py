'''
Created on 22.03.2021

@author: Fernando Penaherrera
'''
import pandas as pd
import matplotlib.pyplot as plt
from common.settings import DATA_DIR


def create_load_profile(peak_electricity=1,
                        peak_dhw=1,
                        peak_heating=1,
                        start_date="2020-01-01",
                        end_date="2020-12-31"):
    raw_data = pd.read_csv(
        DATA_DIR +
        "\\load_profiles\\dummy_energy_demand.csv")
    raw_data["Date"] = raw_data["Date"].apply(
        lambda x: start_date[0:4] + x[4:])
    # change the date to acomodate the year
    raw_data["Date"] = pd.to_datetime(raw_data["Date"])
    raw_data.set_index("Date", inplace=True)
    # get the max of each column
    raw_data_max = raw_data.max()
    max_nominal = {"Electricity": peak_electricity,
                   "DHW": peak_dhw,
                   "Heating": peak_heating,
                   }
    for c in raw_data.columns:
        raw_data[c] = raw_data[c].apply(
            lambda x: x * max_nominal[c] / raw_data_max[c])
    raw_data = raw_data.resample("15T").pad()
    data = raw_data.loc[start_date:end_date]

    return data


if __name__ == '__main__':
    load_profile = create_load_profile(
        peak_electricity=0.5,
        peak_dhw=0.25,
        peak_heating=.66,
        start_date="2019-02-13",
        end_date="2019-03-02")
    load_profile.plot()
    print(load_profile)
    plt.show()
    pass
