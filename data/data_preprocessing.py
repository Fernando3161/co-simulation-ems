'''
Created on 08.03.2021

@author: Fernando Penaherrera
'''
import pandas as pd
import json
import matplotlib.pyplot as plt
import re
from data.data_download import download_data_year
from datetime import datetime
import os.path
from common.settings import DATA_DIR


def convert_german_date(date="30.12.2021"):
    return (date[6:] + "-" + date[3:5] + "-" + date[0:2])


def process_wetter_data(year="2021"):
    '''
    Read txt weather file and return a pandas DF for processing
    :param date:
    '''
    df_list = []
    for month in range(1, 13):
        weather_file_name = DATA_DIR + "\\weather\\weather{}.txt".format(
            year[2:4] + str(month).zfill(2))
        if os.path.isfile(weather_file_name):
            wetter_df = pd.read_csv(weather_file_name, delimiter="\t",
                                    header=None, decimal=",")
            wetter_headings_df = pd.read_csv(
                DATA_DIR + "\\wetter_headings.csv")
            wetter_column_names = wetter_headings_df["Name"].tolist()
            wetter_df.columns = wetter_column_names
            wetter_df["Date"] = wetter_df["Date"].apply(
                lambda x: convert_german_date(x))
            wetter_df["DateTimeStr"] = wetter_df["Date"]\
                + " " + wetter_df["Time"]
            wetter_df["DateTime"] = pd.to_datetime(
                wetter_df["DateTimeStr"], format='%Y-%m-%d %H:%M:%S')
            wetter_df.drop(["Date", "Time", "DateTimeStr"],
                           axis=1, inplace=True)
            wetter_df.rename(columns={"DateTime": "Date"}, inplace=True)
            wetter_df.set_index("Date", inplace=True)
            df_list.append(wetter_df)
    wetter = pd.concat(df_list)
    wetter = wetter.resample("15T").pad()
    wetter.sort_index()
    return wetter


def process_price_data(year="2021"):
    '''
    Process the price data into a DF
    :param date:
    '''
    df_list = []
    for week in range(1, 54):  # there is also week 53 in some cases
        energy_price_data_file = DATA_DIR\
            + "\\generation\\energy_price{}.json".format(
                year[2:4] + "w" + str(week).zfill(2))
        if os.path.isfile(energy_price_data_file):
            with open(energy_price_data_file) as f:
                energy_price_data = json.load(f)
            # The JSON is a list with dictionaries
            # the first ones has the information on the x axis (dates)
            # the "name" has the name of the line that needs to be found
            # for example:
            # "Intraday Continuous Index Price"

            time_info = energy_price_data[0]["xAxisValues"]
            for epd in energy_price_data:
                if epd["name"][0]["en"] == "Intraday Continuous Index Price":
                    intraday_price_data = epd["data"]

                if epd["name"][0]["en"] == "Day Ahead Auction":
                    day_ahead_price_data = epd["data"]

                if epd["name"][0]["en"] == "CO2 Emission Allowances, Auction DE":
                    co2_certificates_price_data = epd["data"]

            dates = [datetime.fromtimestamp(time / 1e3) for time in time_info]
            # this is to filter later by date
            price_dict = {"Date": dates,
                          "Day Ahead Price": day_ahead_price_data,
                          "Intraday Price": intraday_price_data,
                          "CO2 Certificates Price": co2_certificates_price_data
                          }
            price_df = pd.DataFrame(price_dict)
            price_df.set_index("Date", inplace=True)
            df_list.append(price_df)
    price_df_year = pd.concat(df_list)
    price_df_year = price_df_year.resample("15T").pad()
    return price_df_year


def process_generation_data(year="2021"):
    '''
    Process the generation data into a dataframe
    :param date:
    '''
    df_list = []
    for week in range(1, 54):  # there is also week 53 in some cases
        generation_data_file = DATA_DIR\
            + "\\generation\\power_generation{}.json".format(
                year[2:4] + "w" + str(week).zfill(2))
        if os.path.isfile(generation_data_file):
            with open(generation_data_file) as f:
                generation_data = json.load(f)

            # dates data
            time_info = generation_data[0]["xAxisValues"]
            dates = [datetime.fromtimestamp(time / 1e3) for time in time_info]

            generation_dict = {"Date": dates,
                               }
            for gd in generation_data:
                generation_dict[gd["name"][0]["en"]] = gd["data"]

            generation_df = pd.DataFrame(generation_dict)
            generation_df.set_index("Date", inplace=True)
            for l in list(generation_df.columns):
                if "planned" in l or "forecast" in l:
                    generation_df.drop([l], axis=1, inplace=True)
            df_list.append(generation_df)
    generation_df_year = pd.concat(df_list)
    generation_df_year = generation_df_year.resample("15T").pad()
    return generation_df_year


def calculate_specific_co2_emission(year="2021"):
    generation_data = process_generation_data(year)
    co2_data_specific = process_generation_data(year)
    co2_emission_factors = pd.read_csv(DATA_DIR + "\\co2_factors.csv",
                                       comment="#")
    co2_emission_factors.set_index("Energy Source", inplace=True)
    methods = [m for m in co2_emission_factors.columns]
    energy_sources = [c for c in generation_data.columns]

    for m in methods:
        generation_data[m] = 0 * generation_data[energy_sources[0]]
        for c in energy_sources:
            if c in co2_emission_factors.index.values.tolist():
                generation_data[m] += generation_data[c] \
                    * co2_emission_factors.loc[c][m]

    net_generation_columns = [
        'Hydro Power',
        'Biomass',
        'Nuclear',
        'Fossil Brown Coal',
        'Fossil Hard Coal',
        'Fossil Oil',
        'Fossil Gas',
        'Others',
        'Hydro Pumped Storage',
        'Hydro Seasonal Storage',
        'Wind',
        'Solar',
    ]
    generation_data["Total Generation"] = 0 * generation_data[
        energy_sources[0]]
    for n in net_generation_columns:
        if n in list(generation_data.columns):
            generation_data["Total Generation"] += generation_data[n]
    for m in methods:
        co2_data_specific[m] = generation_data[m] / generation_data[
            "Total Generation"]

    for c in co2_data_specific.columns:
        if c not in methods:
            co2_data_specific.drop(c, axis=1, inplace=True)
    return co2_data_specific


def gather_and_process_data(start_date="2021-03-09",
                            end_date="2021-03-15"):
    r = re.compile('.{4}-.{2}-.{2}')
    if r.match(start_date) is None or r.match(end_date) is None:
        print("Wrong date format. Use YYYY-MM-DD")
        return None
    year = start_date[0:4]
    download_data_year(year)
    wetter = process_wetter_data(year).loc[start_date:end_date]
    generation = process_generation_data(year).loc[start_date:end_date]
    price = process_price_data(year).loc[start_date:end_date]
    co2 = calculate_specific_co2_emission(year).loc[start_date:end_date]
    data = pd.concat([wetter, generation, price, co2], axis=1)
    return data


if __name__ == '__main__':
    data = gather_and_process_data("2021-01-06", "2021-01-16")
    data[["Solar Radiation", "LCA Low"]].plot()
    plt.show()
    pass
