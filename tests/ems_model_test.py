'''
Created on 16.04.2021

@author: Fernando Penaherrera (OFFIS/Uni Oldenburg)
'''
from model.ems_model import calculate_scenario
from copy import deepcopy


def run_values_test():
    start_date = "2021-01-16"
    end_date = "2021-01-26"
    size = {
        "Peak_Electricity": 40,
        "Peak_DHW": 10,
        "Peak_Heating": 30,
        "PV": 2000,
        "P2H": 10,
        "Boiler": 30,
        "Heat Storage": 2,
        "Battery": 80, }

    sizes_list = [size]

    # All but 1 is 0
    keys = list(size.keys())
    for k in keys:
        size_t = deepcopy(size)
        size_t[k] = 0
        sizes_list.append(size_t)

    # All 0
    size_0 = deepcopy(size)
    for k in size.keys():
        size_0[k] = 0
    sizes_list.append(size_0)

    # All 0 but 1
    for k in keys:
        size_t = deepcopy(size_0)
        size_t[k] = size[k]
        sizes_list.append(size_t)

    for s in sizes_list:
        try:
            ems = calculate_scenario(start_date, end_date,
                                     s, write_results=False)
            print(s)
            print(ems.kpis)
        except BaseException:
            print("Cannot solve for :")
            print(s)




if __name__ == '__main__':
    run_values_test()
    pass
