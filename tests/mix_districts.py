from oemof import solph

import os
import pandas as pd
import matplotlib.pyplot as plt
from oemof.solph import (EnergySystem, Bus, Sink, Source, Flow,
                         Transformer, GenericStorage, Model)
from copy import deepcopy
from oemof.solph import views, processing


def solve_energy_system(energy_system, res_id):
    model = Model(energy_system)

    model.solve(solver="cbc",
                solve_kwargs={'tee': False},
                solver_io='lp',
                cmdline_options={'ratio': 0.1})

    energy_system.results['valid'] = True
    energy_system.results['main'] = processing.results(
        model)
    energy_system.results['main'] = views.convert_keys_to_strings(
        energy_system.results['main'])
    energy_system.results['meta'] = processing.meta_results(
        model)

    results = energy_system.results['main']
    results_list = []
    for k in results.keys():
        if "flow" in list(results[k]['sequences'].keys()):
            flow = results[k]['sequences']['flow']
            if True:
                key_name = str(k)
                for s in [
                        "(", "'", ")"]:  # remove ( ' ) characters
                    key_name = key_name.replace(s, "")
                flow.rename(key_name, inplace=True)
                flow_df = pd.DataFrame(flow)
                results_list.append(flow_df)
    results = pd.concat(results_list, axis=1)
    results.to_csv("restults{}.csv".format(res_id))
    return results


solver = "cbc"  # 'glpk', 'gurobi',....
debug = False
number_of_time_steps = 24 * 7 * 8
solver_verbose = False  # show/hide solver output

# data sources
filename = os.path.join(os.getcwd(), "basic_example.csv")
data1 = pd.read_csv(filename)
data2 = pd.read_csv(filename)
data2["demand_el"] = data2["demand_el"] * 1.1
data2["pv"] = data2["pv"] * 2
data2["wind"] = data2["wind"] * 3

datas = [data1, data2]
energy_systems = []
nodes = []
exchange_nodes = []
## Create the energy systems
for i in [0, 1]:
    data = datas[i]
    date_time_index = pd.date_range(
        "1/1/2012", periods=number_of_time_steps, freq="H")

    energysystem = solph.EnergySystem(timeindex=date_time_index)

    # Read data file

    bgas = solph.Bus(label="natural_gas%s" % i)
    bel = solph.Bus(label="electricity%s" % i)
    energysystem.add(bgas, bel)
    nodes.append(bgas)
    nodes.append(bel)

    # create excess component for the electricity bus to allow overproduction
    sink = solph.Sink(label="excess_bel%s" % i, inputs={bel: solph.Flow()})

    # create source object representing the natural gas commodity (annual
    # limit)
    sourceGas = solph.Source(
        label="rgas%s" % i,
        outputs={bgas: solph.Flow(nominal_value=29825293, summed_max=1)}, )
    energysystem.add(sink, sourceGas)
    nodes.append(sink)
    nodes.append(sourceGas)

    # create fixed source object representing wind power plants
    sourceWind = solph.Source(
        label="wind%s" % i,
        outputs={bel: solph.Flow(fix=data["wind"], nominal_value=1000000)},)
    sourcePV = solph.Source(
        label="pv%s" % i,
        outputs={bel: solph.Flow(fix=data["pv"], nominal_value=582000)},)

    # create fixed source object representing pv power plants
    energysystem.add(sourceWind, sourcePV)
    nodes.append(sourceWind)
    nodes.append(sourcePV)
    # create simple sink object representing the electrical demand
    sinkDemand = solph.Sink(
        label="demand%s" % i,
        inputs={bel: solph.Flow(fix=data["demand_el"], nominal_value=1)},)
    energysystem.add(sinkDemand)
    nodes.append(sinkDemand)
    # create simple transformer object representing a gas power plant
    gasPP = solph.Transformer(
        label="pp_gas%s" % i,
        inputs={bgas: solph.Flow()},
        outputs={bel: solph.Flow(nominal_value=10e10, variable_costs=50)},
        conversion_factors={bel: 0.58},)
    energysystem.add(gasPP)
    nodes.append(gasPP)

    # create storage object representing a battery
    storage = GenericStorage(
        nominal_storage_capacity=10077997,
        label="storage%s" % i,
        inputs={bel: solph.Flow(nominal_value=10077997 / 6)},
        outputs={
            bel: solph.Flow(nominal_value=10077997 / 6, variable_costs=0.001)},
        loss_rate=0.00,
        initial_storage_level=None,
        inflow_conversion_factor=1,
        outflow_conversion_factor=0.8,)

    energysystem.add(storage)
    nodes.append(storage)

    energy_systems.append(energysystem)

results = []
for i in [0, 1]:
    results.append(solve_energy_system(energy_systems[i], i))

mixed_system = solph.EnergySystem(timeindex=date_time_index)
for n in nodes:
    mixed_system.add(n)

results.append(solve_energy_system(mixed_system, 2))

mixed_system_combined = solph.EnergySystem(timeindex=date_time_index)
for n in nodes:
    mixed_system_combined.add(n)


bus = [n for n in mixed_system_combined.nodes if "electricity" in str(n.label)]
inputs = {
    b: solph.Flow() for b in bus}

demands = [n for n in mixed_system_combined.nodes if "demand" in str(n.label)]
outputs = {
    b: solph.Flow() for b in demands}
bus_inter_districts = solph.Bus(
    label="exchange_districts",
    inputs=inputs,
    outputs=outputs)

mixed_system_combined.add(bus_inter_districts)

results.append(solve_energy_system(mixed_system_combined, 3))


energy_systems.append(mixed_system)
energy_systems.append(mixed_system_combined)

#=========================================================================


i = 0
res_total = pd.DataFrame()
for result in results:
    c = [c for c in result.columns if "exchange" in c]
    print(c)
    for c in c:
        res_total["{}-{}".format(c, i)] = result[c]
    i += 1

res_total.to_csv("results.csv")
res_total.plot()
plt.show()
