'''
Created on 22.03.2021

@author: Fernando Penaherrera
'''
# @PydevCodeAnalysisIgnore
from data.data_preprocessing import gather_and_process_data
from data.load_profiles import create_load_profile
from oemof.solph import (EnergySystem, Bus, Sink, Source, Flow,
                         Transformer, GenericStorage, Model)
from components.photovoltaic import PVpanel
from functions.conversion import kJ_to_kWh
from oemof.solph import views, processing
from copy import deepcopy
from oemof.thermal import stratified_thermal_storage as sts
import pandas as pd
from math import pi
from common.settings import DATA_DIR, RESULTS_DIR
import matplotlib.pylab as plt
from datetime import datetime
from model.visualization import plot_from_file
from functions.auxiliar import get_fraction_of_hour_timeseries
'''
This model contains a small system with:
-Electricity Demand
-Heat Demand
-Electric Supply from:
    Grid
    PV
    Battery
-Heat Supply from:
    Gas burner
    P2H
    Thermal Tank

Conversion from Electricity to heat is possible
The model CAN optimise with regards to:
Price Information, also considering CO2
CO2 Emissions

This will serve also as test for a dynamic price selection

Units need to be in kW
'''

START = "2021-02-10"
END = "2021-02-15"
TEMP_STORAGE = 60  # C
TEMP_REFERENCE = 0  # C
H2O_DENSITY = 1000  # kg/m3
H2O_HEAT_CAPACITY = 4.182  # kJ/kg/K
TC_INSULATION = 0.04  # W / (m * K)
# EUR/kg https://www.globalpetrolprices.com/Germany/natural_gas_prices/
GAS_PRICE = 0.052 * 13.50  # EUR/kWh,
# https://www.volker-quaschning.de/datserv/CO2-spez/index_e.php
GAS_EMISSION = 0.23
GAS_LHV = 13.50  # kWh/kg
# https://www.engineeringtoolbox.com/fuels-higher-calorific-values-d_169.html
EEG_INCENTIVE = 65  # EUR/MW

# IPCC,IPCC LCA,BMWI,UBA,LCA Mean,LCA Low,LCA High
CO2_CALCULATION_METHOD = "LCA Mean"
INCLUDE_CO2_COSTS = False


class EnergySystemModel:

    def __init__(self,
                 start_date=START,
                 end_date=END,
                 write_results=True,
                 **kwargs
                 ):

        # Get data from sizing
        self.peak_electricity = kwargs.get("Peak_Electricity")
        self.peak_dhw = kwargs.get("Peak_DHW")
        self.peak_heating = kwargs.get("Peak_Heating")
        self.pv = kwargs.get("pv")
        self.p2h = kwargs.get("p2h")
        self.gas_boiler = kwargs.get("gas_boiler")
        self.battery = kwargs.get("battery")
        self.heat_storage = kwargs.get("heat_storage")

        self.write_results = write_results
        self.locals = locals()
        self.start_date = start_date
        self.end_date = end_date
        self.co2_cost_factor = 1 if INCLUDE_CO2_COSTS else 0

        # Modelling steps
        self.get_data()
        self.calculate_energy_costs()
        self.build_energy_system()
        self.get_solve_model()
        self.calculate_results_and_kpi()
        self.write_input_outputs()

    def get_data(self):

        file_name = "{} to {}-Boundary_Data.csv".format(
            self.start_date, self.end_date)

        import os.path
        # check if exists
        self.demand_data = create_load_profile(
            peak_electricity=self.peak_electricity,  # kW
            peak_dhw=self.peak_dhw,  # kW
            peak_heating=self.peak_heating,  # kW
            start_date=self.start_date,
            end_date=self.end_date)

        if os.path.exists(RESULTS_DIR + "//" + file_name):
            boundary_data = pd.read_csv(RESULTS_DIR + "//" + file_name)
            boundary_data['Date'] = pd.to_datetime(
                boundary_data['Date'], format="%Y-%m-%d %H:%M:%S")
            boundary_data.set_index("Date", inplace=True)
            boundary_data.index.freq = boundary_data.index.inferred_freq
            self.environmental_data = boundary_data[
                boundary_data.columns.tolist()[0:-3]]

        else:
            self.environmental_data = gather_and_process_data(
                start_date=self.start_date,
                end_date=self.end_date)

            boundary_data = pd.concat(
                [self.environmental_data, self.demand_data], axis=1)
            boundary_data.to_csv(RESULTS_DIR + "\\" + file_name)

        self.HF = get_fraction_of_hour_timeseries(self.demand_data)

    def calculate_energy_costs(self):
        self.electricity_costs = deepcopy(
            self.environmental_data[["Day Ahead Price"]])  # EUR/MWh
        emission_costs = deepcopy(
            self.environmental_data[["CO2 Certificates Price",
                                     CO2_CALCULATION_METHOD]])
        # EUR/tonCO2 *tonCO2/MWh
        emission_costs["emission costs"] = self.co2_cost_factor\
            * emission_costs["CO2 Certificates Price"]\
            * self.environmental_data[CO2_CALCULATION_METHOD]  # EUR/tonCO2

        self.electricity_costs["total elec costs"] = self.electricity_costs[
            "Day Ahead Price"]\
            + emission_costs["emission costs"]
        self.gas_price = deepcopy(
            self.environmental_data[["CO2 Certificates Price"]])
        self.gas_price["gas_price"] = self.gas_price.apply(
            lambda x: GAS_PRICE +
            GAS_EMISSION *
            GAS_LHV *
            self.gas_price["CO2 Certificates Price"] *
            self.co2_cost_factor /
            1000)
        print("gas_price:", self.gas_price)
        # EUR/kg + kgCO2/kWh*kWh/kg*EUR/tonCO2/1000

    def build_energy_system(self,):
        self.energy_system = EnergySystem(timeindex=self.demand_data.index)

        # Buses
        b_el_in = Bus(label="b_el_in")
        b_pv = Bus(label="b_pv",)
        b_renewable = Bus(label="b_renewable", inputs={b_pv: Flow()})
        b_el_out = Bus(
            label="b_el_out",
            inputs={
                b_renewable: Flow(
                    variable_costs=-
                    EEG_INCENTIVE *
                    self.HF /
                    1000)})
        b_electric_supply = Bus(label="b_electric_supply",
                                inputs={b_renewable: Flow(),
                                        b_el_in: Flow()})
        b_p2h = Bus(label="b_p2h",
                    inputs={b_electric_supply: Flow()})
        b_heat_gas = Bus(label="b_gas")
        b_heat_grid = Bus(label="b_heat_grid")
        b_heat_supply = Bus(label="b_heat_supply",
                            inputs={b_heat_grid: Flow()})
        self.energy_system.add(
            b_el_in,
            b_electric_supply,
            b_pv,
            b_renewable,
            b_el_out,
            b_p2h,
            b_heat_gas,
            b_heat_grid,
            b_heat_supply)

        s_electric_grid = Source(
            label="s_electric_grid",
            outputs={
                b_el_in: Flow(
                    variable_costs=self.electricity_costs["total elec costs"] *
                    self.HF /
                    1000)})

        s_electric_out = Sink(
            label="s_electric_out",
            inputs={b_el_out: Flow()})  # here change for EEG prices

        s_gas = Source(
            label='m_gas',
            outputs={
                b_heat_gas: Flow(
                    variable_costs=self.gas_price["gas_price"] *
                    self.HF)})
        print(self.gas_price["gas_price"] * self.HF)
        # Create local energy demand
        d_el = Sink(label='d_el',
                    inputs={b_electric_supply: Flow(
                        fix=self.demand_data['Electricity'],
                        nominal_value=1
                    )})
        d_heat = Sink(label='d_heat',
                      inputs={b_heat_supply: Flow(
                          fix=self.demand_data['Heating'],
                          nominal_value=1
                      )})

        d_dhw = Sink(label='d_dhw',
                     inputs={b_heat_supply: Flow(
                         fix=self.demand_data['DHW'],
                         nominal_value=1)})

        self.energy_system.add(s_electric_grid, s_electric_out,
                               s_gas, d_el, d_heat, d_dhw)

        # Technologies

        # PHOTOVOLTAIK
        pv = self.pv

        pv_flows = deepcopy(self.environmental_data[["Solar Radiation"]])
        pv_flows["DateTimeString"] = [
            x.strftime("%Y-%m-%d %H:%M:%S") for x in pv_flows.index]
        pv_flows["Production, kW"] = pv_flows.apply(lambda x: PVpanel(
            lat=pv["Lat"],
            area=pv["Area"],
            efficiency=pv["Efficiency"],
            el_tilt=pv["Elevation"],
            az_tilt=pv["Azimuth"],
            start_date=x["DateTimeString"]
        ).power(x["Solar Radiation"]), axis=1)
        s_pv = Source(label="s_pv",
                      outputs={b_pv: Flow(nominal_value=1,
                                          max=pv_flows["Production, kW"])})

        # P2H
        p2h = self.p2h

        t_p2h = Transformer(
            label='t_p2h', inputs={
                b_p2h: Flow()}, outputs={
                b_heat_grid: Flow(
                    nominal_value=p2h["Size"])}, conversion_factors={
                    b_p2h: 1, b_heat_grid: p2h["Efficiency"]})

        # GAS BOILER
        gas_boiler = self.gas_boiler

        t_boiler = Transformer(
            label='t_boiler',
            inputs={b_heat_gas: Flow()},
            outputs={
                b_heat_grid: Flow(nominal_value=gas_boiler['Power'])},
            conversion_factors={
                b_heat_gas: 1 / GAS_LHV,
                b_heat_grid: gas_boiler['Efficiency']})

        self.energy_system.add(t_p2h, t_boiler, s_pv)

        # BATTERY
        battery = self.battery

        sto_battery = GenericStorage(
            label='sto_battery',
            inputs={
                b_pv: Flow(nominal_value=battery["power"])},
            outputs={
                b_renewable: Flow(nominal_value=battery["power"])},
            loss_rate=0 * battery["self_discharge"],
            nominal_storage_capacity=battery["capacity"],
            inflow_conversion_factor=battery["efficiency_inflow"],
            outflow_conversion_factor=battery["efficiency_outflow"],
            initial_storage_level=0,
            balanced=False)

        # WATER TANK (HEAT STORAGE)
        heat_storage = self.heat_storage

        hs_capacity = heat_storage['volume'] * \
            kJ_to_kWh((heat_storage['temp_storage'] - TEMP_REFERENCE) *
                      H2O_DENSITY *
                      H2O_HEAT_CAPACITY)
        hs_loss_rate, hs_fixed_losses_relative, hs_fixed_losses_absolute = \
            sts.calculate_losses(
                u_value=TC_INSULATION / heat_storage['insulation_thickness'],
                diameter=heat_storage['diameter'],
                temp_h=heat_storage['temp_storage'],
                temp_c=TEMP_REFERENCE,
                temp_env=self.environmental_data['Temperature'])

        sto_heat = GenericStorage(
            label="sto_heat",
            inputs={b_heat_grid: Flow(nominal_value=heat_storage["power"])},
            outputs={b_heat_grid: Flow(nominal_value=heat_storage["power"])},
            nominal_storage_capacity=hs_capacity,
            loss_rate=hs_loss_rate,
            fixed_losses_absolute=hs_fixed_losses_absolute,
            fixed_losses_relative=hs_fixed_losses_relative
        )

        self.energy_system.add(sto_battery, sto_heat)

    def get_solve_model(self):

        model = Model(self.energy_system)

        import time
        start = time.time()
        model.solve(solver="cbc",
                    solve_kwargs={'tee': False},
                    solver_io='lp',
                    cmdline_options={'ratio': 0.1})
        end = time.time()
        # print("Time to solve: " + str(round(end - start, 2)) + " Seconds")

        self.energy_system.results['valid'] = True
        self.energy_system.results['main'] = processing.results(
            model)
        self.energy_system.results['main'] = views.convert_keys_to_strings(
            self.energy_system.results['main'])
        self.energy_system.results['meta'] = processing.meta_results(
            model)


    def _results_processing(self):
        results = self.energy_system.results['main']
        results_list = []
        for k in results.keys():
            if "flow" in list(results[k]['sequences'].keys()):
                flow = results[k]['sequences']['flow']
                if True:
                    key_name = str(k)
                    for s in [
                            "(", "'", ")"]:  # remove ( ' ) characters
                        key_name = key_name.replace(s, "")
                    flow.rename(key_name, inplace=True)
                    flow_df = pd.DataFrame(flow)
                    results_list.append(flow_df)
        self.results = pd.concat(results_list, axis=1)

    def _calculate_kpis(self):
        kpis = pd.DataFrame(index=self.results.index)
        co2_emission_factors = pd.read_csv(
            DATA_DIR + "\\co2_factors.csv", comment="#")
        co2_emission_factors.set_index("Energy Source", inplace=True)
        pv_emission_factor = co2_emission_factors.loc["Solar"][
            CO2_CALCULATION_METHOD]

        # CO2s Emission
        kpis["CO2_electricity"] = self.results["b_el_in, b_electric_supply"] * \
            self.environmental_data["BMWI"] * self.HF
        kpis["CO2_pv"] = self.results["s_pv, b_pv"] * \
            pv_emission_factor * self.HF  # TEMPORAL VALUE
        kpis["CO2_gas"] = self.results["m_gas, b_gas"] * \
            GAS_EMISSION * GAS_LHV * self.HF  # kgCO2/kWh*kWh/kg
        kpis["CO2"] = kpis["CO2_electricity"] + \
            kpis["CO2_pv"] + kpis["CO2_gas"]

        # Energy Costs
        kpis["Cost_Electricity"] = self.results["s_electric_grid, b_el_in"] * \
            self.electricity_costs["total elec costs"] * self.HF / 1000
        kpis["Cost_Electricity_Sales"] = - \
            self.results["b_el_out, s_electric_out"] * \
            EEG_INCENTIVE * self.HF / 1000
        kpis["Cost_Gas_Purchases"] = self.results["m_gas, b_gas"] * \
            self.gas_price["gas_price"] * self.HF
        kpis["Cost"] = kpis["Cost_Electricity"] + \
            kpis["Cost_Electricity_Sales"] + kpis["Cost_Gas_Purchases"]

        # Local Consumption
        kpis["RE_Generation"] = self.results["b_pv, b_renewable"] * \
            self.HF + self.results["sto_battery, b_renewable"] * self.HF
        kpis["RE_Use"] = self.results[
            "b_renewable, b_electric_supply"] * self.HF
        kpis["Self_Consumption"] = kpis["RE_Use"] / kpis["RE_Generation"]

        # Self Sufficiency
        kpis["PV_Power"] = self.results["s_pv, b_pv"]
        kpis["Elec2Heat"] = self.results["b_electric_supply, b_p2h"]
        kpis["Elec2Elec"] = self.results["b_electric_supply, d_el"]
        kpis["Self_sufficiency"] = kpis["PV_Power"] / \
            (kpis["Elec2Heat"] + kpis["Elec2Elec"])

        self.kpi_series = kpis

    def calculate_results_and_kpi(self):
        self._results_processing()
        self._calculate_kpis()
        self.results = pd.concat([self.results, self.kpi_series], axis=1)

        self.dt_string = datetime.now().strftime("%Y%m%d-%H%M%S")

        if self.write_results:
            self.results_file_name = "{} - Results.csv".format(self.dt_string)
            self.results.to_csv(RESULTS_DIR + "//" + self.results_file_name)

    @property
    def kpis(self):
        kpis_total = {
            "CO2, kgCo2": round(self.kpi_series["CO2"].sum(), 1),  # kgCO2
            # EUR
            "Energy Costs, EUR": round(self.kpi_series["Cost"].sum(), 1),
            "Self Consumption": round(
                self.kpi_series["RE_Use"].sum()
                / self.kpi_series["RE_Generation"].sum(), 3),
            "Self Sufficiency": round(
                self.kpi_series["PV_Power"].sum() / (
                    self.kpi_series["Elec2Heat"].sum() +
                    self.kpi_series["Elec2Elec"].sum()), 4),
            "Total Electricity, kWh": round(
                self.results["s_electric_grid, b_el_in"].sum() * self.HF, 2),
            "Total Gas, m3": round
            (self.results["m_gas, b_gas"].sum() * self.HF, 2),
        }

        return kpis_total

    def write_input_outputs(self):
        if self.write_results:
            dates = {"Start": self.start_date,
                     "End": self.end_date, }
            size = self.locals["kwargs"]
            kpis = self.kpis
            import json
            dall = {}
            for d in [dates, size, kpis]:
                dall.update(d)
            json_name = RESULTS_DIR + "//" + "{} - Summary.json".format(
                self.dt_string)
            with open(json_name, 'w') as fp:
                json.dump(dall, fp)


def generate_technologies_dict(size):
    pv = {"Area": size["PV"],
          "Efficiency": 0.23,
          "Lat": 55,
          "Elevation": 10,
          "Azimuth": 30
          }

    p2h = {"Size": size["P2H"],
           "Efficiency": 1
           }

    gas_boiler = {
        'Efficiency': 0.85,
        'Power': size["Boiler"],  # kW
    }
    battery = {'power': 10,  # kW
               'capacity': size["Battery"],  # kWh
               'efficiency_inflow': 0.98,
               'efficiency_outflow': 0.98,
               'self_discharge': 1E-6 / 4
               }

    heat_storage = {'volume': size["Heat Storage"],  # m3 4 * 4 * 4 * pi / 4
                    'insulation_thickness': 0.010,  # m
                    'diameter': 4,  # m
                    'temp_storage': 60,  # C,
                    'power': 100  # kW, #rule of thumb? 20*volume
                    }

    technologies = {
        "Peak_Electricity": size["Peak_Electricity"],
        "Peak_DHW": size["Peak_DHW"],
        "Peak_Heating": size["Peak_Heating"],
        "pv": pv,
        "p2h": p2h,
        "gas_boiler": gas_boiler,
        "battery": battery,
        "heat_storage": heat_storage
    }
    return technologies


def calculate_scenario(start_date, end_date, size, write_results=False):
    technologies = generate_technologies_dict(size)
    return EnergySystemModel(
        start_date,
        end_date,
        write_results,
        **technologies)


def calculate_and_visualize_results(start_date, end_date, size):
    ems = calculate_scenario(start_date, end_date, size, write_results=True)
    for k,v in ems.kpis.items():
        print(k, ": ", v)
    plot_from_file(RESULTS_DIR + "//" + ems.results_file_name)



if __name__ == '__main__':
    # Which properties do I want to change
    start_date = "2021-01-16"
    end_date = "2021-01-26"
    size = {
        "Peak_Electricity": 40,
        "Peak_DHW": 10,
        "Peak_Heating": 30,
        "PV": 2000,
        "P2H": 10,
        "Boiler": 30,
        "Heat Storage": 2,
        "Battery": 80, }

    calculate_and_visualize_results(start_date, end_date, size)

#===============================================================================
#     pv_size = [100 * x for x in range(0, 21)]
#     el_size = [20 * x for x in range(1, 6)]
#     res = {"el_d": el_size,
#            "CO2, kgCo2": [],
#            "Energy Costs, EUR": [],
#            "Self Sufficiency": [],
#            "Self Consumption": [],
#            "Total Electricity, kWh": [],
#            "Total Gas, m3": [], }
# 
#     for el in el_size:
#         size["Peak_Electricity"] = el
#         ems = calculate_scenario(start_date, end_date, size)
# 
#         for k, v in ems.kpis.items():
#             res[k].append(ems.kpis[k])
# 
#     res_df = pd.DataFrame.from_dict(res)
#     res_df.to_csv(
#         "{} Sensitivity.csv".format(
#             RESULTS_DIR +
#             "//" +
#             ems.dt_string))
#     res_df.plot.scatter("Total Electricity, kWh", "Energy Costs, EUR")
#     plt.show()    # 12600
#===============================================================================
