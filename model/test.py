'''
Created on 08.04.2021

@author: Fernando Penaherrera
'''
import pandas as pd

# datetime object containing current date and time

s = pd.date_range(start='2018-04-24 00:00:00',
                  end='2018-04-24 23:45:00', periods=24 * 4)
print(s.inferred_freq)
