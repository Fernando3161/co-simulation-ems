'''
Created on 07.04.2021

@author: Fernando Penaherrera
'''
from datetime import datetime
import matplotlib.pyplot as plt
import mpld3
import pandas as pd
from mpld3 import plugins
from matplotlib.dates import DateFormatter
from mpld3._server import serve
import numpy as np
from matplotlib.ticker import FormatStrFormatter

css = """
table
{
  border-collapse: collapse;
}
th
{
  color: #ffffff;
  background-color: #000000;
}
td
{
  background-color: #cccccc;
}
table, th, td
{
  font-family:Arial, Helvetica, sans-serif;
  border: 1px solid black;
  text-align: right;
}
"""


def plot_from_file(filename):
    data_groups = get_data_groups_from_file(filename)
    html = build_html_graphs(data_groups)
    serve(html)
    return None


def get_data_groups_from_file(filename):
    data = pd.read_csv(filename)
    data["Date"] = [datetime.strptime(x, "%Y-%m-%d %H:%M:%S")
                    for x in data["Date"]]
    data.set_index("Date", inplace=True)

    heat_list = [x for x in data.columns if "heat" in x]
    renew_list = [x for x in data.columns if "renew" in x]
    sto_list = [x for x in data.columns if "sto" in x]
    elec_list = [x for x in data.columns if "b_el" in x]
    kpi_list = ["CO2",
                "Cost",
                "Self_Consumption",
                "Self_sufficiency"]
    data_groups = {
        "Heat": data[heat_list],
        "Renewable Sources": data[renew_list],
        "Storage": data[sto_list],
        "Electrical Grid": data[elec_list],
        "KPIs": data[kpi_list]
    }
    return data_groups


def get_fig_as_html(key, value):
    data = value
    fig, ax = plt.subplots(figsize=(12, 4))
    lines = []
    pointers = []
    labels = list(data.columns)

    for c in labels:
        line = ax.plot(data[c],
                       marker=".",
                       markersize=2,
                       )
        tt = plugins.PointLabelTooltip(
            line[0],
            labels=[str(round(x, 0)) for x in data[c].values.tolist()])
        lines.append(line)
        pointers.append(tt)
    plugins.connect(fig,
                    plugins.InteractiveLegendPlugin(lines,
                                                    labels,
                                                    alpha_unsel=0,
                                                    alpha_over=0.5),
                    *pointers)

    ax.set_xlabel("Date")
    ax.set_ylabel("Power (kW)")
    ax.set_title(key)
    ax.xaxis_date()
    ax.xaxis.set_major_formatter(DateFormatter('%Y-%m-%d %I:%M %p'))

    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
    return mpld3.fig_to_html(fig)


def build_violin_chart(key, value):
    data_kpi = value
    fig, axs = plt.subplots(1, 4, figsize=(12, 4))
    fig.suptitle("KPIs")

    i = 0
    pointers = []
    for c in value.columns:

        data = data_kpi[c].sort_values().dropna()

        parts = axs[i].violinplot(
            data, showmeans=False, showmedians=False,
            showextrema=False)

        for pc in parts['bodies']:
            pc.set_facecolor(tuple(np.random.randint(256, size=3) / 256))
            pc.set_edgecolor('black')
            pc.set_alpha(0.5)

        quartile1, medians, quartile3 = np.percentile(data, [25, 50, 75])
        whiskers = np.array([adjacent_values(data, quartile1, quartile3)])[0]
        whiskers_min, whiskers_max = whiskers[0], whiskers[1]

        inds = np.arange(1, 2)
        axs[i].vlines(inds, quartile1, quartile3,
                      color='grey', linestyle='-', lw=5)
        axs[i].vlines(inds, whiskers_min, whiskers_max,
                      color='grey', linestyle='-', lw=1)

        points = [whiskers_min, quartile1, medians, quartile3, whiskers_max]
        scatter0 = axs[i].plot(1, data.mean(),
                               marker="o", color="white", zorder=3,
                               markersize=10)
        tt0 = plugins.PointHTMLTooltip(
            scatter0[0], labels=["Mean :" + str(round(data.mean(), 2))])

        scatter1 = axs[i].plot([1] * len(points), points,
                               marker="o", color="black", zorder=3)
        axs[i].set_title(c)

        axs[i].xaxis.set_major_formatter(FormatStrFormatter('%.0f'))
        names = ["LAV", "Q1", "Median", "Q3", "UAV"]
        labels = [n + ": " + str(round(x, 2)) for n, x in zip(names, points)]

        tt1 = plugins.PointHTMLTooltip(scatter1[0], labels=labels)
        pointers.append(tt0)
        pointers.append(tt1)

        i += 1

    plugins.connect(fig, *pointers)
    fig.subplots_adjust(hspace=0.4)
    return mpld3.fig_to_html(fig)


def adjacent_values(vals, q1, q3):
    upper_adjacent_value = q3 + (q3 - q1) * 1.5
    upper_adjacent_value = np.clip(upper_adjacent_value, q3, vals[-1])

    lower_adjacent_value = q1 - (q3 - q1) * 1.5
    lower_adjacent_value = np.clip(lower_adjacent_value, vals[0], q1)
    return lower_adjacent_value, upper_adjacent_value


def build_html_graphs(data_groups):
    html = None
    for k, v in data_groups.items():
        if "KPI" not in k:
            if html is None:
                html = get_fig_as_html(k, v)
            else:
                html = html + get_fig_as_html(k, v)

        elif "KPI" in k:
            html = html + build_violin_chart(k, v)

    return html


if __name__ == '__main__':
    plot_from_file("20210415-151045 - Results.csv")
    pass
